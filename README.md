# Test Driven Development (TDD)
- Een manier van programmeren waarbij tests je ontwikkeling sturen.
- Geformaliseerd door Kent Beck in het eind van de jaren '90.

### Voordelen van TDD?
- Het dwingt je eerst na te denken over requirements.
- Het dwingt je eerst na te denken over de interface van je software.
- Het dwingt je testbare code te schrijven.
- Het begeleidt je programmeerwerk, zodat je alleen schrijft wat nodig is.
- Het stelt je in staat sneller bugs op te sporen, of zelfs te voorkomen.<br>
  TDD kost in het begin meer tijd, maar dit win je hier later mee terug.
- Het zorgt ervoor dat er tests zijn die mede dienen als documentatie.
- Het zorgt ervoor dat alles getest is, dus dat je vertrouwen hebt in je software.
- Refactoren gaat makkelijker.

### Nadelen van TDD?
- Het werkt langzamer.
- Je kunt het alleen toepassen wanneer de requirements vast liggen.
- Als de interface onderhevig is aan verandering, neemt TDD meer tijd in beslag.
- Als een lid van het team geen TDD toepast, is de garantie die TDD belooft weg.
  Iedereen moet het toepassen.

## De drie regels van TDD
<div class="rules">
  <div class="center">
    <ol>
     <li><strong>Schrijf geen productiecode voordat je een falende test hebt.</strong></li>
     <li><strong>Schrijf niet meer testcode dan nodig om een test te laten falen.</strong> (Compilatie fouten horen hier bij.)</li>
     <li><strong>Schrijf niet meer productiecode dan nodig om de test te laten slagen.</strong></li>
    </ol>
  </div>
</div>

Bij iedere iteratie van deze drie regels, moeten er twee dingen gebeuren.
1. De testcode wordt specifieker.
2. De productiecode wordt generieker. (Maar niet generieker dan nodig.)

##### Don't go for the gold!
Ga niet gelijk voor de kern van het probleem. Wacht zolang je kunt, en test eerst de bijzaken.

Aan het eind van het traject, rolt de oplossing van de kern er makkelijk uit.

## Beginnen met TDD
Misschien wil je snel beginnen met TDD op het werk. Er is grote kans dat je zult falen.
Je zult waarschijnlijk een hekel krijgen aan TDD!

Het eerste dat je nodig hebt is oefening.

## Katas
Een kata is een herhaalbare oefening. Het woord komt uit oosterse (Japanse) vechtkunsten.
Je voert een aantal handelingen herhaaldelijk uit totdat je de vaardigheid onder de knie hebt.

Hier zijn de drie regels van TDD die herhaaldelijke handelingen.

Je kunt hier TDD katas vinden (en ook andere katas): https://kata-log.rocks/tdd

<style>
  .rules {
    border: solid;
  }
  .center {
    margin: auto;
    max-width: 500px; 
  }
</style>